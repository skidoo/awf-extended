#!/bin/bash
# debian: sudo apt install dpkg-dev devscripts build-essential dh-make dh-autoreconf intltool libgtk-3-dev



cd "$(dirname "$0")"
gtk="gtk3"
version="2.2.0"
rm -rf builder/

# copy to a tmp directory
mkdir builder
mkdir builder/awf-${gtk}-${version}
touch builder/awf-${gtk}-${version}/NEWS
touch builder/awf-${gtk}-${version}/AUTHORS
touch builder/awf-${gtk}-${version}/README
touch builder/awf-${gtk}-${version}/ChangeLog
cp -r ../icons/             builder/awf-${gtk}-${version}/
cp -r ../src/               builder/awf-${gtk}-${version}/
cp awf-${gtk}.desktop       builder/awf-${gtk}-${version}/
cp ../Makefile.am           builder/awf-${gtk}-${version}/
cp ../configure.ac          builder/awf-${gtk}-${version}/
cp /usr/share/common-licenses/GPL-3 builder/awf-${gtk}-${version}/COPYING
sed -i 's/ -eq 2/ -eq -1/g' builder/awf-${gtk}-${version}/configure.ac
sed -i 's/ -eq 4/ -eq -1/g' builder/awf-${gtk}-${version}/configure.ac
for file in builder/awf-${gtk}-${version}/icons/*/*/*; do mv $file ${file/\/awf./\/awf-${gtk}.}; done
for file in builder/awf-${gtk}-${version}/src/po/*.po; do
	lang=$(basename "$file" .po)
	mkdir -p builder/awf-${gtk}-${version}/locale/${lang}/LC_MESSAGES
	msgfmt   builder/awf-${gtk}-${version}/src/po/${lang}.po -o builder/awf-${gtk}-${version}/locale/${lang}/LC_MESSAGES/awf-${gtk}.mo
done

cd builder/
tar czf awf-${gtk}-${version}.tar.gz awf-${gtk}-${version}/
cd ..

# create packages for debian and ubuntu
for serie in unstable hirsute groovy focal bionic xenial trusty precise; do

	if [ $serie = "unstable" ]; then
		# ubuntu only
		cp -a builder/awf-${gtk}-${version}/ builder/awf-${gtk}-${version}+src/
		# debian only
		cd builder/awf-${gtk}-${version}/
		dh_make -s -y -f ../awf-${gtk}-${version}.tar.gz
	else
		# ubuntu only
		cp -a builder/awf-${gtk}-${version}+src/ builder/awf-${gtk}-${version}+${serie}/
		cd builder/awf-${gtk}-${version}+${serie}/
		dh_make -s -y -f ../awf-${gtk}-${version}.tar.gz
	fi

	rm -f debian/*ex debian/*EX debian/README* debian/*doc*
	mkdir debian/upstream
	cp ../../control   debian/
	cp ../../changelog debian/
	cp ../../copyright debian/
	cp ../../install   debian/
	cp ../../watch     debian/
	cp ../../rules     debian/
	cp ../../upstream  debian/upstream/metadata

	if [ $serie = "unstable" ]; then
		dpkg-buildpackage -us -uc
	else
		# debhelper: unstable:13 hirsute:13 groovy:13 focal:12 bionic:9 xenial:9 trusty:9 precise:9
		if [ $serie = "focal" ]; then
			sed -i 's/debhelper-compat (= 13)/debhelper-compat (= 12)/g' debian/control
		fi
		if [ $serie = "bionic" ]; then
			sed -i 's/debhelper-compat (= 13)/debhelper-compat (= 9)/g' debian/control
		fi
		if [ $serie = "xenial" ]; then
			sed -i 's/debhelper-compat (= 13)/debhelper (>= 9)/g' debian/control
			sed -i ':a;N;$!ba;s/Rules-Requires-Root: no\n//g' debian/control
			echo 9 > debian/compat
		fi
		if [ $serie = "trusty" ]; then
			sed -i 's/debhelper-compat (= 13)/debhelper (>= 9)/g' debian/control
			sed -i ':a;N;$!ba;s/Rules-Requires-Root: no\n//g' debian/control
			echo 9 > debian/compat
		fi
		if [ $serie = "precise" ]; then
			sed -i 's/debhelper-compat (= 13)/debhelper (>= 9)/g' debian/control
			sed -i ':a;N;$!ba;s/Rules-Requires-Root: no\n//g' debian/control
			echo 9 > debian/compat
		fi
		sed -i 's/unstable/'${serie}'/g' debian/changelog
		sed -i 's/-1) /-1+'${serie}') /' debian/changelog
		dpkg-buildpackage -us -uc -ui -d -S
	fi
	cd ..

	echo "==========================="
	if [ $serie = "unstable" ]; then
		# debian only
		debsign awf-${gtk}_${version}-*.changes
		echo "==========================="
		lintian -EviIL +pedantic awf-${gtk}_${version}-*.deb
	else
		# ubuntu only
		debsign awf-${gtk}_${version}*+${serie}*source.changes
	fi
	echo "==========================="
	cd ..
done

ls -dltrh $PWD/builder/*.deb $PWD/builder/*.changes
echo "==========================="

# cleanup
rm -rf builder/awf-${gtk}-${version}*/ builder/awf-${gtk}-${version}*.tar.gz